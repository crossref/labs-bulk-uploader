

###Crossref Colors###

CR_PRIMARY_COLORS = {
    "CR_PRIMARY_RED": "#ef3340",
    "CR_PRIMARY_GREEN": "#3eb1c8",
    "CR_PRIMARY_YELLOW": "#ffc72c",
    "CR_PRIMARY_LT_GREY": "#d8d2c4",
    "CR_PRIMARY_DK_GREY": "#4f5858",
}

###Crossref URLs###

API_URI = "https://api.crossref.org/"
TEST_DEPOSIT_URL = "https://test.crossref.org/servlet/deposit"
PROD_DEPOSIT_URL = "https://doi.crossref.org/servlet/deposit"
PROD_LOGIN_URL = "https://doi.crossref.org/servlet/login"


###Assets###

CROSSREF_LOGO = "https://assets.crossref.org/logo/crossref-logo-landscape-200.png"
CROSSREF_ICON = "https://assets.crossref.org/logo/crossref-logo-200.svg"