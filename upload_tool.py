import argparse,os
import datetime
import requests


# def upload_process(mydata,environment,filepath):
# 	for filename in os.listdir(filepath):
# 		f = os.path.join(filepath, filename)
# 		with open(f,"r") as file:
# 			myfile = {'fname':file}
# 			r = requests.post(environment, files=myfile, data=mydata)
# 			if r.status_code == 200:
# 				print(f"{filename} was successfully sent to the queue")
# 			else:
# 				print(f"The file {filename} has failed to reach the queue, please try again later")


def headers():
  return {
      "User-Agent" : "crossref-labs-file-uploader; mailto=support@crossref.org"
  }



def upload_process(credentials,environment):
	for f in args.files:
		print(f"---------- {f.name} ------------- ")
		content = f.read()
		deposit_file(credentials,environment,content)
		



def deposit_file(credentials,environment,file):
	file_upload = {'fname':file}
	r = requests.post(environment, files=file_upload, data=credentials,headers=headers())
	if r.status_code == 200:
		print(f"The file was successfully sent to the queue") 
		return True
	else:
		print(f"The file has failed to reach the queue, please try again later")
	return False


# def get_filenames(filepath):
# 	filename_list = []
# 	for filename in os.listdir(filepath):
# 		f = os.path.join(filepath, filename)
# 		filename_list.append(f)
# 	return filename_list


def form_data(operation,username,password,role=None):
	
	if role == None:
		credentials = {
		'operation':operation,
		'login_id': username,
		'login_passwd': password,

		}
	else:
		credentials = {
		'operation':operation,
		'login_id': f"{username}/{role}",
		'login_passwd': password,

		}
	return credentials

def timestamp():
	return ('{:%Y-%m-%d-%H-%M}'.format(datetime.datetime.now()))


if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='Crossref file uploader')
	parser.add_argument('-username','-u', type=str,help='enter an "email address/role" or just "role"', required=True)
	parser.add_argument('-password','-p', type=str,help='enter a password', required=True)
	parser.add_argument('-files', type=argparse.FileType("r"), nargs="+",help='files to upload',required=True)
	parser.add_argument('-operation','-o', type=str,help='enter an operation e.g."doMDUpload"', required=True)
	parser.add_argument('-system_url','-s', type=str,help='enter the URL for the env to POST to e.g. "test" or "production".', required=True)
	args = parser.parse_args()
	# filename_list = get_filenames(args.filepath)
	upload_process(form_data(args.operation,args.username,args.password),args.system_url,args.files)