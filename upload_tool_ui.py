import pandas as pd
import streamlit as st
import requests
import datetime
from urllib.parse import unquote
import xml.etree.cElementTree as ET
from bs4 import BeautifulSoup as bs
from collections import Counter
import os
import upload_tool
import new_sidebar
from settings import PROD_LOGIN_URL, CROSSREF_ICON, TEST_DEPOSIT_URL,PROD_DEPOSIT_URL,API_URI,CROSSREF_LOGO
st.set_page_config(page_title="Labs Upload Tool", page_icon=CROSSREF_ICON, layout="wide")
st.subheader("**Crossref Labs File Upload Tool**")
st.markdown("####")
st.markdown("####")


def login():
	mydata = {
			'usr': st.session_state.email,
			'pwd': st.session_state.password,
		}
	s = requests.Session()
	st.session_state.r = s.post(PROD_LOGIN_URL,data=mydata).json()
	# st.write(st.session_state.r)
	if st.session_state.r['authenticated'] == True:
		authenticated()
		return True
	elif st.session_state.r['authenticated'] == False:
			st.error("Your username or password are incorrect")
			return False

def authenticated():
	st.session_state.roles_list = []
	try:
		roles = st.session_state.r['roles']
		if roles:
			for item in roles:
				st.session_state.roles_list.append(item)
				st.session_state.authenticated = True
			return st.session_state.roles_list
	except KeyError:
		return False
	
	
def upload_types_dict():
	return {
		"Metadata Upload" : "doMDUpload",
		"URL updates" : "doTransferDOIsUpload" 

		}

def logout():
	for key in st.session_state.keys():
		del st.session_state[key]


def environ_url(env):
	if env == "Test":
		return TEST_DEPOSIT_URL 
	elif env == "Production":
		return PROD_DEPOSIT_URL


def upload_form():
	with st.sidebar.container():
		st.success(f"You are logged in as {st.session_state.email} on {st.session_state.system_choice}")
		st.button("Logout", on_click=logout)
	form_body = st.form("File Uploader")###,clear_on_submit=True)
	form_body.selectbox("Username/role for upload",key="role",options=sorted(st.session_state.roles_list))
	form_body.radio("Upload Type",upload_types_dict().keys(),key="upload_type",index=1,horizontal=True)
	form_body.markdown('##')
	upload_files = form_body.file_uploader("Upload the files to deposit",type=['xml','txt'], accept_multiple_files=True,key="files")
	if upload_files:
		st.session_state.filename_list = []
		for u in st.session_state.files:
			st.session_state.filename_list.append(u.name)
	form_body.form_submit_button("Deposit Files",on_click=deposit_files)
	
def deposit_files():
	results = st.container()
	successful_deposit = 0
	sub_log_results = results.empty()
	for f in st.session_state.files:
		deposited = upload_tool.deposit_file(upload_tool.form_data(upload_types_dict().get(st.session_state.upload_type),st.session_state.email,st.session_state.password,st.session_state.role),environ_url(st.session_state.system_choice),f)
		if deposited == True:
			successful_deposit += 1
	sub_log_results.success(f"Successfully sent to queue = {successful_deposit}/{len(st.session_state.files)}:white_check_mark: ")###{nl}Failed to send = {len(failures)}/{len(upload_files)}")


if __name__ == "__main__":
	# st.session_state
	st.sidebar.image(CROSSREF_LOGO)
	form_info = st.empty()
	email_box = st.sidebar.empty()
	pass_box = st.sidebar.empty()
	env_choice = st.sidebar.empty()
	log_but = st.sidebar.empty()
	email = email_box.text_input("Email",key="email",help="Enter your Crossref email here")
	password = pass_box.text_input("Password",key="password",type="password",help="Enter your Crossref password here")
	system_choice = env_choice.radio("Login to which environment (default is 'Production')", ["Production","Test"],index=0,key='system_choice',horizontal=True,help="Do you want to login to the production environment or the test environment")
	login_button = log_but.button("Login")
	form_info.info("Login to the uploader tool using your Crossref credentials on the left")
	if password:
		if login():
			email_box.empty()
			pass_box.empty()
			log_but.empty()
			env_choice.empty()
			form_info.empty()
			if st.session_state.authenticated == True:
				upload_form()

			
